import peasy.*;
import peasy.org.apache.commons.math.*;
import peasy.org.apache.commons.math.geometry.*;

float x = 1;
float y = 0;
float z = 0;

float a = 10;
float b = 28;
float c = 8 / 3;

ArrayList<PVector> points = new ArrayList<PVector>();

PeasyCam cam;

void setup() {
  size(800, 600, P3D);
  colorMode(HSB);
  cam = new PeasyCam(this, 1000);
}

void draw() {
  background(0);
  float dt = 0.01;
  float dx = (a * (y - x)) * dt;
  float dy = (x * (b - z) - y) * dt;
  float dz = (x * y - c * z) * dt;
  x += dx;
  y += dy;
  z += dz;
  
  points.add(new PVector(x,y,z));
  
  rotateZ(PI / 3);
  scale(7);
  stroke(255);
  
  noFill();
  float hu = 0;
  beginShape();
  for (PVector v : points) {
    stroke(hu, 255, 255);
    vertex(v.x, v.y, v.z);
    hu = (hu > 255) ? 0 : hu + 1;
  }
  endShape();
}